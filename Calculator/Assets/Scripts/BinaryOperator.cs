﻿using System.Collections;

public abstract class BinaryOperator : Item
{
    public abstract double Compute(double inputOne, double inputTwo);
}
