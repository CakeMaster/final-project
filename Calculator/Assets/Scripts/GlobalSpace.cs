﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum AngleType { Degrees, Radians, Gradians }

namespace GlobalSpace
{
    public static class Display
    {
        public const char STX = (char)2;
        public const char ETX = (char)3;

        public static readonly char SI = (char)17;
        public static readonly char SO = (char)16;

        public static string AppendOperatorSyntax(string s)
        {
            return STX + s + ETX;
        }

        private static Dictionary<object, string> OperatorDisplayDictionaryItemToString = new Dictionary<object, string>();
        //{
        //    { new Plus(), "+" },
        //    { new Multiply(), "*" }
        //};
        private static Dictionary<string, object> OperatorDisplayDictionaryStringToItem = new Dictionary<string, object>();
        //{
        //    { "+", new Plus() },
        //    { "*", new Multiply() }
        //};

        static Display()
        {
            // AddToOperatorDisplayDictionary(ITEM, STRING);
            AddToOperatorDisplayDictionary(new Sin(), "sin");
            AddToOperatorDisplayDictionary(new Cos(), "cos");
            AddToOperatorDisplayDictionary(new Tan(), "tan");
            AddToOperatorDisplayDictionary(new Csc(), "csc");
            AddToOperatorDisplayDictionary(new Sec(), "sec");
            AddToOperatorDisplayDictionary(new Cot(), "cot");
            AddToOperatorDisplayDictionary(new Asin(), "Asin");
            AddToOperatorDisplayDictionary(new Acos(), "Acos");
            AddToOperatorDisplayDictionary(new Atan(), "Atan");
            AddToOperatorDisplayDictionary(new Acsc(), "Acsc");
            AddToOperatorDisplayDictionary(new Asec(), "Asec");
            AddToOperatorDisplayDictionary(new Acot(), "Acot");
            AddToOperatorDisplayDictionary(new Log(), "log");
            AddToOperatorDisplayDictionary(new Ln(), "ln");
            AddToOperatorDisplayDictionary(new Root(), "√");
            AddToOperatorDisplayDictionary(new Square(), "²");
            AddToOperatorDisplayDictionary(new Cube(), "³");
            AddToOperatorDisplayDictionary(new Invert(), "^(-1)");
            AddToOperatorDisplayDictionary(new Plus(), "+");
            AddToOperatorDisplayDictionary(new Minus(), "-");
            AddToOperatorDisplayDictionary(new Multiply(), "*");
            AddToOperatorDisplayDictionary(new Divide(), "/");
            AddToOperatorDisplayDictionary(new Decimal(), ".");
            AddToOperatorDisplayDictionary(new Power(), "^");
            AddToOperatorDisplayDictionary(new LogBase(), "logbase");
        }

        public static void AddToOperatorDisplayDictionary(object item, string s)
        {
            if (LookUp(s) != null)
                return;
            OperatorDisplayDictionaryItemToString.Add(item, s);
            OperatorDisplayDictionaryStringToItem.Add(s, item);
        }

        public static object LookUp(string s)
        {
            //Debug.Log(OperatorDisplayDictionaryStringToItem);
            if (OperatorDisplayDictionaryStringToItem.ContainsKey(s))
                return OperatorDisplayDictionaryStringToItem[s];
            else
                return null;
        }
        public static string LookUp(object item)
        {
            if (OperatorDisplayDictionaryItemToString.ContainsKey(item))
                return OperatorDisplayDictionaryItemToString[item];
            else
                return null;
        }

        public static List<Item> ParseString(string s)
        {
            char STX = GlobalSpace.Display.STX;
            char ETX = GlobalSpace.Display.ETX;

            List<Item> list = new List<Item>();

            int index = 0;
            while (index < s.Length)
            {
                if (Char.IsNumber(s[index]))
                {
                    string num = "";
                    for (; (index < s.Length && Char.IsNumber(s[index])); index++)
                        num += s[index];

                    list.Add(new Number(double.Parse(num)));
                }
                else if (s[index] == '(')
                {
                    index++;
                    list.Add(new ParenOpen());
                }
                else if (s[index] == ')')
                {
                    index++;
                    list.Add(new ParenClose());
                }
                else if (s[index] == 'e')
                {
                    index++;
                    list.Add(new E());
                }
                else if (s[index] == 'π')
                {
                    index++;
                    list.Add(new Pi());
                }
                else if (s[index] == 'x')
                {
                    index++;
                    list.Add(new X());
                }
                else if (s[index] == STX)
                {
                    index++;

                    string op = "";
                    for (; s[index] != ETX; index++)
                        op += s[index];

                    list.Add((GlobalSpace.Display.LookUp(op) as Item).Clone() as Item);

                    index++;
                }
            }
            return AddImpliedOperators(list);
        }

        public static List<Item> AddImpliedOperators(List<Item> itemList)
        {
            Item previous = null;
            for (int i = 0; i < itemList.Count; i++)
            {
                if (itemList[i] is Number)
                { 
                    if (itemList[i] is Pi || itemList[i] is E || itemList[i] is X)
                    {
                        if (i + 1 < itemList.Count && itemList[i + 1] is Number)
                            itemList.Insert(i + 1, new Multiply());

                        if (previous is Number)
                        {
                            itemList.Insert(i, new Multiply());
                            i++;
                        }
                    }

                    if (i + 1 < itemList.Count)
                    {
                        if (itemList[i + 1] is ParenOpen)
                            itemList.Insert(i + 1, new Multiply());
                        else if (itemList[i + 1] is Operator)
                        {
                            Operator op = itemList[i + 1] as Operator;

                            bool takesPrefixArgument = false;
                            foreach (int arg in op.arguments)
                                if (arg < 0)
                                    takesPrefixArgument = true;

                            if (! takesPrefixArgument)
                                itemList.Insert(i + 1, new Multiply());
                        }
                    }

                    if (previous is ParenClose)
                    {
                        itemList.Insert(i, new ParenOpen());
                        i++;
                    }
                }

                if (itemList[i] is ParenClose && i + 1 < itemList.Count && itemList[i + 1] is ParenOpen)
                    itemList.Insert(i + 1, new Multiply());

                previous = itemList[i];
            }
        //    DebugItemList(itemList);
            return itemList;
        }

        public static void DebugItemList(List<Item> itemList)
        {
            foreach (Item i in itemList)
            {
                string debugString = "null";

                if (i is Number)
                    debugString = "number";
                if (i is Operator)
                    debugString = "operator";
                if (i is ParenOpen)
                    debugString = "parenopen";
                if (i is ParenClose)
                    debugString = "parenclose";

                Debug.Log(debugString);
            }
        }
    }


    public static class Global
    {
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T ParseEnum<T>(this string value, T defaultValue)
        {
            if (!Enum.IsDefined(typeof(T), value))
                return defaultValue;

            return (T)Enum.Parse(typeof(T), value);
        }
        /* public static string NumberStringToSuperScript(string num)
           {
               string superScriptNum = "";
               for (int i = 0; i < num.Length; i++)
               {
                   switch(num[i])
                   {
                       case '1':
                           superScriptNum += '\u00B9';
                           break;
                       case '2':
                           superScriptNum += '\u00B2';
                           break;
                       case '3':
                           superScriptNum += '\u00B3';
                           break;
                       case '4':
                           superScriptNum += '\u2074';
                           break;
                       case '5':
                           superScriptNum += '\u2075';
                           break;
                       case '6':
                           superScriptNum += '\u2076';
                           break;
                       case '7':
                           superScriptNum += '\u2077';
                           break;
                       case '8':
                           superScriptNum += '\u2078';
                           break;
                       case '9':
                           superScriptNum += '\u2079';
                           break;
                       case '0':
                           superScriptNum += '\u2070';
                           break;
                   }
               }
           }
           */

    }

    public static class Settings
    {
        public static AngleType AngleForTrig = AngleType.Degrees;
    }

    public static class Constants
    {
        // public static double DegreeToRadian(double input)  { return input * 0.0174532925199; }
        // public static double GradianToRadian(double input) { return input * 0.01570796326795;  }

        public const double DegreeToRadian  = 0.0174532925199;
        public const double DegreeToGradian = 0.0157079632680;

        public static double ConvertToRadians(double input)
        {
            switch (Settings.AngleForTrig)
            {
                case AngleType.Degrees:
                    return input * DegreeToRadian;
                case AngleType.Radians:
                    return input;
                case AngleType.Gradians:
                    return input * DegreeToGradian;
                default:
                    throw new InvalidProgramException();
            }
        }

        public static double ConvertFromRadiansToCurrent(double input)
        {
            switch (Settings.AngleForTrig)
            {
                case AngleType.Degrees:
                    return input / DegreeToRadian;
                case AngleType.Radians:
                    return input;
                case AngleType.Gradians:
                    return input / DegreeToGradian;
                default:
                    throw new InvalidProgramException();
            }
        }
    }
}
