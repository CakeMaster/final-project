﻿using UnityEngine;
using System.Collections.Generic;

public static class Calculator {

    static List<Item> itemList;

    public static bool IsFormattedCorrectly()
    {
        // Check same number ParenOpen and ParenClose
        int parenOpen = 0;
        int parenClose = 0;
        foreach(Item i in itemList)
        {
            if (i is ParenOpen)
                parenOpen++;

            if (i is ParenClose)
                parenClose++;
        }

        if (parenOpen != parenClose)
            return false;

        // Satisfies all criteria.
        return true;
    }

    public static bool ContainsX()
    {
        foreach (Item i in itemList)
        {
            if (i is X)
                return true;
        }

        return false;
    }

    public static void ReplaceXWithDouble(double d)
    {
        foreach (Item i in itemList)
        {
            if (i is X)
                (i as X).X_Value = d;
        }
    }

    public static void SetItemList(List<Item> list)
    {
        itemList = list;
    }

    public static string Calculate()
    {
        if (!IsFormattedCorrectly())
            return "ERROR";

        foreach (Item i in itemList)
        {
            if (i is X)
                return "See Graph";
        }

        int highestParenOpen = -1;
        for (int i = 0; i < itemList.Count; i++)
            if (itemList[i] is ParenOpen)
                highestParenOpen = i;
            else if (itemList[i] is ParenClose)
            {
                // a b c d e f g h
                // 0 1 2 3 4 5 6 7 8 9
                // op = 2
                // i  = 7

                itemList.RemoveAt(i); // remove close paren
                itemList.RemoveAt(highestParenOpen); // remove open paren
                Item item = Calculate(itemList.GetRange(highestParenOpen, i - highestParenOpen - 1));
                itemList.RemoveRange(highestParenOpen + 1, i - highestParenOpen - 2);
                itemList[highestParenOpen] = item;

                if (item == null)
                    return "ERROR";

                return Calculate();
            }

        Number result = Calculate(itemList) as Number;
        return (result != null) ? formatResult(result) : "ERROR";
    }

    public static double CalculateWithX(double x)
    {
        if (!IsFormattedCorrectly())
            return 0;

        ReplaceXWithDouble(x);



        int highestParenOpen = -1;
        for (int i = 0; i < itemList.Count; i++)
            if (itemList[i] is ParenOpen)
                highestParenOpen = i;
            else if (itemList[i] is ParenClose)
            {
                itemList.RemoveAt(i); // remove close paren
                itemList.RemoveAt(highestParenOpen); // remove open paren
                Item item = Calculate(itemList.GetRange(highestParenOpen, i - highestParenOpen - 1));
                itemList.RemoveRange(highestParenOpen + 1, i - highestParenOpen - 2);
                itemList[highestParenOpen] = item;

                if (item == null)
                    return 0;

                return CalculateWithX(x);
            }

        Number result = Calculate(itemList) as Number;
        return (result == null) ? 0 : (result is X) ? (result as X).X_Value : result.Value;
    }

    private static string formatResult(Number result)
    {
        double value = result.Value;
        return value.ToString((value > Mathf.Pow(10, 9)) ? "E5" :"N3");
    }

    private static string formatResult(double result)
    {
        return result.ToString((result > Mathf.Pow(10, 9)) ? "E5" : "N3");
    }

    public static Item Calculate(List<Item> list)
    {
        if (list.Count == 0)
            return new Number(0);

        if (list.Count == 1 && list[0] is Number)
            return list[0];

        while (list.Count > 1)
            if (! PerformHighestOrderOperation(list))
                return null;
          
        return list[0];
    }

    public static bool PerformHighestOrderOperation(List<Item> list)
    {
        int index = -1;
        Operator highestOrderOperator = null;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] is Operator)
            {
                Operator op = (list[i] as Operator);

                if (op.CheckIfComputable(list, i) &&
                   (highestOrderOperator == null || op.order > highestOrderOperator.order))
                {
                    index = i;
                    highestOrderOperator = op;       
                }
            }
        }

        if (highestOrderOperator == null)
            return false;
        else
        {
            highestOrderOperator.Computer(list, index);
            Debug.Log("Computed");
        }

        return true;
    }

    /* public void PerformHighestOrderOperation(List<Item> list)
    {
        int index = -1;
        int highestOrder = -1;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] is Operator)
            {
                Operator o = (list[i] as Operator);
                if (o.order > highestOrder)
                {
                    foreach (int num in o.arguments)
                    {
                        if (!(list[i + num] is Number))
                            break;
                        index = i;
                        highestOrder = o.order;
                    }
                }
            }
        }
        Operator op = list[index] as Operator;
        List<double> args = new List<double>();
        foreach (int num in op.arguments)
            args.Add((list[index + num] as Number).Value);

        Number result = new Number(op.Compute(args));
        list[index] = result;

        for (int i = op.arguments.Count - 1; i >= 0; i--)
            list.RemoveAt(index + op.arguments[i]);
    } */


}
