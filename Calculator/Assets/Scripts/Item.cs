﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public abstract class Item : ICloneable
{
    public abstract object Clone();
    public abstract string Display();
}
