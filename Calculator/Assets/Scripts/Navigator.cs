﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using GlobalSpace;

public class Navigator : MonoBehaviour
{
    private InputManager inputManager;

    private InputField mainInputField; // not initialized
    int caretPositionOnReturn = 0;

    public List<Item> itemList;

    EventSystem system;

    void Start()
    {
        system = EventSystem.current;
        inputManager = GameObject.Find("InputManager").GetComponent<InputManager>();

        // mainInputField = GameObject.Find("DisplayManager").GetComponent<DisplayManager>().MainInputField;
    }

    public void Enter(bool moveOn = false)
    {
        if (moveOn)
            caretPositionOnReturn++;             // not just this, move to next item

        inputManager.SetAsSelectedInputField(mainInputField, caretPositionOnReturn);
    }  

    // Update is called once per frame
    void Update()
    {
        if (mainInputField.isFocused == false)
            return;

        int caretPosition = -1;
        string text = mainInputField.text;

        if (UnityEngine.Input.GetKeyDown(KeyCode.LeftArrow))
        {
            caretPosition = mainInputField.caretPosition;// + 1;

            if (caretPosition == 0)
                return;
            // a S c d E f
            //0 1 2 3 4 5 6
            if (text[caretPosition] == GlobalSpace.Display.ETX)
                while (text[caretPosition] != GlobalSpace.Display.STX)
                    caretPosition--;
        }

        if (UnityEngine.Input.GetKeyDown(KeyCode.RightArrow))
        {
            caretPosition = mainInputField.caretPosition;// - 1;

            if (caretPosition == text.Length)
                return;

            if (text[caretPosition - 1] == GlobalSpace.Display.STX)
                while (text[caretPosition - 1] != GlobalSpace.Display.ETX)
                    caretPosition++;
        }

        if (caretPosition != -1)
        {
            inputManager.SetCaret(caretPosition);
            mainInputField.caretPosition = caretPosition;
            mainInputField.text = text;
        }
    }
}