﻿using System;
using System.Collections;
using System.Collections.Generic;
using GlobalSpace;

public abstract class Operator : Item
{
    public abstract double Compute(List<double> input);
    private string display;

    public List<int> arguments { get; private set; }
    public int order { get; private set; }
    
    public Operator(int order, List<int> arguments, string display = "?")
    {
        this.arguments = arguments;
        this.order = order;
        this.display = display;

        //if (GlobalSpace.Display.LookUp(display) == null)
        //    GlobalSpace.Display.AddToOperatorDisplayDictionary(this.MemberwiseClone(), display);
    }

    public override object Clone()
    {
        return this.MemberwiseClone();
    }

    public bool CheckIfComputable(List<Item> itemList, int index)
    {
        foreach (int arg in arguments)
        {
            if (itemList.Count <= index + arg)
                return false;

            if (!(itemList[index + arg] is Number))
                return false;
        }

        return true;
    }
    //   a b c + d
     //  0 1 2 3 4
     //  count = 5
    public void Computer(List<Item> itemList, int index)
    {
        List<double> argumentList = new List<double>();

        foreach (int arg in arguments)
            argumentList.Add((itemList[index + arg] is X) ? (itemList[index + arg] as X).X_Value : (itemList[index + arg] as Number).Value);

        Number result = new Number(Compute(argumentList));

        itemList[index] = result;

        for (int i = arguments.Count - 1; i >= 0; i--)
            itemList.RemoveAt(index + arguments[i]);
    }

    public override string Display()
    {
        // return GlobalSpace.Display.AppendOperatorSyntax(display);

        string displayString = GlobalSpace.Display.LookUp(this);
        if (displayString != null)
            return displayString;
        else
            return "";

       // GlobalSpace.Display.LookUp("a").GetType().
    }
}

//public abstract class LargeOperator : Operator
//{
//    public abstract override double Compute(List<double> input);

//    public List<List<Item>> definitions { get; set; }

//    public LargeOperator(int order, List<int> arguments, string display = "?") : base(order, arguments, display)
//    {
//        definitions = new List<List<Item>>();
//    } 
//}