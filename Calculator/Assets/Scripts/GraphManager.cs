﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class GraphManager : MonoBehaviour {

    const double SETTINGS_XMIN = -21;
    const double SETTINGS_XMAX = 21;
    const double SETTINGS_XRANGE = SETTINGS_XMAX - SETTINGS_XMIN;

    const double SETTINGS_INCREMENTS = 50.0;
    const double SETTINGS_DRANGE = SETTINGS_XRANGE / SETTINGS_INCREMENTS;

    const double SETTINGS_YMIN = -25;
    const double SETTINGS_YMAX = 50;
    const double SETTINGS_YRANGE = SETTINGS_YMAX - SETTINGS_YMIN;

    const double SETTINGS_LINEX_MAX = 217;
    const double SETTINGS_LINEX_MIN = -217;
    const double SETTINGS_LINEY_MAX = 122;
    const double SETTINGS_LINEY_MIN = -128;

    const double SETTINGS_LINEX_RANGE_INCREMENT = (SETTINGS_LINEX_MAX - SETTINGS_LINEX_MIN) / SETTINGS_INCREMENTS;
    // const double SETTINGS_LINEY_RANGE_INCREMENT = (SETTINGS_LINEY_MAX - SETTINGS_LINEY_MIN) / SETTINGS_INCREMENTS;


    public InputField XMin;
    public InputField XMax;
    public InputField XSpacing;

    public InputField YMin;
    public InputField YMax;
    public InputField YSpacing;

    public LineRenderer Line;

    public double TextToDouble(InputField t)
    {
        double d;
        if (double.TryParse(t.text, out d))
            return d;
        else
            return -1;
    }

    // Use this for initialization
    void Start () {
        XMin.text = "-5";
        XMax.text = "5";
        XSpacing.text = "1";
        YMin.text = "-5";
        YMax.text = "5";
        YSpacing.text = "1";
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Graph(List<Item> list)
    {

        // Need to copy itemList
        List<Item> duplicateItemList = new List<Item>();
        foreach (Item i in list)
            duplicateItemList.Add(i.Clone() as Item);


        double max_X = TextToDouble(XMax);
        double min_X = TextToDouble(XMin);
        double increment = (max_X - min_X) / SETTINGS_INCREMENTS;

        int vertexCount = 0;
        Line.SetVertexCount(0);

        for (double i = 0; SETTINGS_LINEX_MIN + SETTINGS_LINEX_RANGE_INCREMENT * i < SETTINGS_LINEX_MAX; i++)
        {
            double result = Calculator.CalculateWithX(min_X + increment * i);

            List<Item> revert = new List<Item>();
            foreach (Item j in duplicateItemList)
                revert.Add(j.Clone() as Item);
            Calculator.SetItemList(revert);

            vertexCount++;
            Line.SetVertexCount(vertexCount);
            // need make it in ui plane
            Line.SetPosition((vertexCount - 1), new Vector3((float) (SETTINGS_LINEX_MIN + SETTINGS_LINEX_RANGE_INCREMENT * i), (float) result * 20, -0.1f));
        }
    }

    //public void PlotLine(double x1, double y1, double x2, double y2)
    //{
    //    GameObject LR = Instantiate(Line);
    //}
}
