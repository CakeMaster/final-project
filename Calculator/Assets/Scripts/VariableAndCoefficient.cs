﻿using System.Collections;

public struct VariableAndCoefficient
{
    public double Coefficient { get; set; }
    public double VariablePower { get; set; }
}
