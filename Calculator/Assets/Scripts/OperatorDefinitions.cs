﻿using System;
using System.Collections.Generic;
using GlobalSpace;
using UnityEngine;
/*
    Delete, Clear, NavigateLeft, NavigateRight, 
    ParenLeft, ParenRight,
    Summation, Derivative, Integral,
    Answer, Equals,
*/

// UNIARY OPERATORS

public class Sin : Operator
{
    public Sin() : base(7, new List<int> { 1 }, "sin")
    {
        ;
    }
    public override double Compute(List<double> input)
    {      
        return Math.Sin(Constants.ConvertToRadians(input[0]));
    }
}
public class Cos : Operator
{
    public Cos() : base(7, new List<int> { 1 }, "cos")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Cos(Constants.ConvertToRadians(input[0]));
    }
}
public class Tan : Operator
{
    public Tan() : base(7, new List<int> { 1 }, "tan")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Tan(Constants.ConvertToRadians(input[0]));
    }
}
public class Csc : Operator
{
    public Csc() : base(7, new List<int> { 1 }, "csc")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / Math.Sin(Constants.ConvertToRadians(input[0]));
    }
}
public class Sec : Operator
{
    public Sec() : base(7, new List<int> { 1 }, "sec")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / Math.Cos(Constants.ConvertToRadians(input[0]));
    }
}
public class Cot : Operator
{
    public Cot() : base(7, new List<int> { 1 }, "cot")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / Math.Tan(Constants.ConvertToRadians(input[0]));
    }
}

public class Asin : Operator
{
    public Asin() : base(7, new List<int> { 1 }, "Asin")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Constants.ConvertFromRadiansToCurrent(Math.Asin(/*Constants.ConvertToRadians(*/input[0]/*)*/));
    }
}
public class Acos : Operator
{
    public Acos() : base(7, new List<int> { 1 }, "Acos")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Constants.ConvertFromRadiansToCurrent(Math.Acos(/*Constants.ConvertToRadians(*/input[0]/*)*/));
    }
}
public class Atan : Operator
{
    public Atan() : base(7, new List<int> { 1 }, "Atan")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Constants.ConvertFromRadiansToCurrent(Math.Atan(Constants.ConvertToRadians(input[0])));
    }
}
public class Acsc : Operator
{
    public Acsc() : base(7, new List<int> { 1 }, "Acsc")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Constants.ConvertFromRadiansToCurrent(Math.Asin(1 / Constants.ConvertToRadians(input[0])));
    }
}
public class Asec : Operator
{
    public Asec() : base(7, new List<int> { 1 }, "Asec")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / Constants.ConvertFromRadiansToCurrent(Math.Acos(1 / Constants.ConvertToRadians(input[0])));
    }
}
public class Acot : Operator
{
    public Acot() : base(7, new List<int> { 1 }, "Acot")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / Math.Atan(1 / Constants.ConvertFromRadiansToCurrent(Constants.ConvertToRadians(input[0])));
    }
}

public class Log : Operator
{
    public Log() : base(8, new List<int> { 1 }, "log")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Log10(input[0]);
    }
}
public class Ln : Operator
{
    public Ln() : base(8, new List<int> { 1 }, "ln")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Log(input[0], Math.E);
    }
}

public class Root : Operator
{
    public Root() : base(9, new List<int> { 1 }, "√")
    {
        GlobalSpace.Display.AddToOperatorDisplayDictionary(this, "√");
    }
    public override double Compute(List<double> input)
    {
        return Math.Sqrt(input[0]);
    }
}
public class Square : Operator
{
    public Square() : base(9, new List<int> { -1 }, "²")
    {
        GlobalSpace.Display.AddToOperatorDisplayDictionary(this, "²");
    }
    public override double Compute(List<double> input)
    {
        return input[0] * input[0];
    }
}
public class Cube : Operator
{
    public Cube() : base(9, new List<int> { -1 }, "³")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return input[0] * input[0] * input[0];
    }
}
public class Invert : Operator
{
    public Invert() : base(9, new List<int> { 1 }, "^(-1)")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return 1 / input[0];
    }
}


// BINARY OPERATORS

public class Plus : Operator
{
    public Plus() : base(6, new List<int> { -1, 1 }, "+")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return input[0] + input[1];
    }
}
public class Minus : Operator
{
    public Minus() : base(6, new List<int> { -1, 1 }, "-")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return input[0] - input[1];
    }
}
public class Multiply : Operator
{
    public Multiply() : base(8, new List<int> { -1, 1 }, "*")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return input[0] * input[1];
    }
}
public class Divide : Operator
{
    public Divide() : base(8, new List<int> { -1, 1 }, "/")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return input[0] / input[1];
    }
}
public class Decimal : Operator
{
    public Decimal() : base(10, new List<int> { -1, 1 }, ".")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        double toAppend = input[1];

        while (toAppend >= 1f)
            toAppend = toAppend / 10f;

        return input[0] + toAppend;
    }
}
public class Power : Operator
{
    public Power() : base(9, new List<int> { -1, 1 }, "^")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Pow(input[0], input[1]);
    }
}

public class LogBase : Operator
{
    public LogBase() : base(1, new List<int> { 1, 2 }, "logbase")
    {
        ;
    }
    public override double Compute(List<double> input)
    {
        return Math.Log(input[0], input[1]);
    }
}