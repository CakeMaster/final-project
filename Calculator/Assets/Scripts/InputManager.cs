﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GlobalSpace;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public enum Input {
    Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, TripleZero,
    X, E, Pi, 
    Decimal,
    Plus, Minus, Multiply, Divide,
    Root, Square, Cube, Power,
    Sin, Cos, Tan, Csc, Sec, Cot,
    Asin, Acos, Atan, Acsc, Asec, Acot,
    Log, LogBase, Ln, Invert, PowerE, Power10,
    Delete, Clear, NavigateLeft, NavigateRight, 
    ParenLeft, ParenRight,
    Summation, Derivative, Integral,
    Answer, Equals,
    ERROR
}

public class InputManager : MonoBehaviour {

    public DisplayManager DisplayManager;
    public GraphManager GraphManager;

    // public InputField ActiveInputField { get; set; }
    public InputField ActiveInputField;

    private int caretPosition;

    private string CalcuatedInputText = "";

    List<LargeOperatorNavigator> largeOperatorNavigatorList = new List<LargeOperatorNavigator>();

    EventSystem system;

    public void SetAsSelectedInputField(InputField inputField, int setCaretPosition = 0)
    {
        inputField.OnPointerClick(new PointerEventData(system));
        StartCoroutine(FollowedUpdate(inputField, setCaretPosition));
    }

    public void SetCaret(InputField inputField, int position)
    {
        FollowedUpdate(inputField, position);
    }

    public void SetCaret(int position)
    {
        SetCaret(ActiveInputField, position);
    }

    IEnumerator FollowedUpdate(InputField inputField, int setCaretPosition = 0)
    {
        yield return new WaitForEndOfFrame();

        inputField.selectionAnchorPosition = setCaretPosition;
        inputField.selectionFocusPosition = setCaretPosition;

        yield break;       
    }

    void Start()
    {
        system = EventSystem.current;
        caretPosition = 0;

        //Set();
        DisplayToResultText();
    }

    public void SetAngleTypeDegrees()
    {
        Settings.AngleForTrig = AngleType.Degrees;
    }

    public void SetAngleTypeRadians()
    {
        Settings.AngleForTrig = AngleType.Radians;
    }

    public void SetAngleTypeGradians()
    {
        Settings.AngleForTrig = AngleType.Gradians;
    }

    void Update()
    {
        if (ActiveInputField.text != CalcuatedInputText)
        {
            Debug.Log("A: " + ActiveInputField.text + "  CD: " + CalcuatedInputText);

            DisplayToResultText();
            CalcuatedInputText = ActiveInputField.text;
        }
    }

    /*public void DisplayToInputText()
    {
        List<Item> inputList = GlobalSpace.Display.ParseString(ActiveInputField.text);
        displayedText = ActiveInputField.text;

        DisplayManager.UpdateDisplayList(inputList);      
    } */

    public void DisplayToResultText()
    {
        List<Item> inputList = GlobalSpace.Display.ParseString(ActiveInputField.text);

        string p = "";
        foreach (Item i in inputList)
            p += "ITEM: " + i.GetType().ToString() + " " + ((i is X) ? (i as X).X_Value : ((i is Number) ? (i as Number).Value : -1000)) + "\n";
        Debug.Log("List: " + p);

        // Calculator c = new Calculator();
        Calculator.SetItemList(inputList);

        GraphManager.Graph(inputList);


        DisplayManager.UpdateResult(Calculator.Calculate());
    }

    //public void Set()
    //{
    //    inputList.Add(new Number(1));
    //    inputList.Add(new Plus());
    //    inputList.Add(new ParenOpen());
    //    inputList.Add(new Root());
    //    inputList.Add(new Number(1));
    //    inputList.Add(new Plus());
    //    inputList.Add(new Number(1));
    //    inputList.Add(new Plus());
    //    inputList.Add(new ParenOpen());
    //    inputList.Add(new Number(3));
    //    inputList.Add(new Multiply());
    //    inputList.Add(new Number(4));
    //    inputList.Add(new Divide());
    //    inputList.Add(new ParenOpen());
    //    inputList.Add(new Number(2));
    //    inputList.Add(new Decimal());
    //    inputList.Add(new Number(5));
    //    inputList.Add(new ParenClose());
    //    inputList.Add(new ParenClose());
    //    inputList.Add(new ParenClose());
    //    inputList.Add(new Square());

    //    //Calculator c = new Calculator();
    //    //c.SetItemList(inputList);
    //    //Debug.Log(c.Calculate());
    //}

    public void NumericalInput(string num)
    {
        ActiveInputField.text += num;
    }

    public void OperatorInput(string op)
    {
        ActiveInputField.text += GlobalSpace.Display.AppendOperatorSyntax(op);
    }

    public void Delete()
    {

    }
}
