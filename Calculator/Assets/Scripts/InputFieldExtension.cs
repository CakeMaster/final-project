﻿using GlobalSpace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;
using UnityEngine.UI;


class InputFieldExtension : InputField
{
    private int caretTracker = 0;

    protected override void Start()
    {
        base.Start();
    }

    void Update()
    {
        caretPosition = caretTracker;
    }

    public void Clear()
    {
        text = "";
        caretTracker = 0;
    }

    public void Delete()
    {
        if (caretTracker == 0)
            return;

        if (text[caretTracker - 1] == Display.ETX)
        {
            int startLocation = caretTracker - 1;
            while (text[startLocation] != Display.STX)
                startLocation--;

            text = text.Remove(startLocation, caretTracker - startLocation);
            caretTracker -= caretTracker - startLocation;
        }
        else
        {
            text = text.Remove(caretTracker - 1, 1);
            caretTracker--;
        }
    }

    public void AcceptInputNumber(string input)
    {
        if (text.Length == 0)
        {
            text += input;
            caretTracker += input.Length;
        }
        else
        {
            text = text.Insert(caretTracker, input);
            caretTracker += input.Length;
        }
    }

    public void AcceptInputOperator(string input)
    {
        AcceptInputNumber(Display.AppendOperatorSyntax(input));
    }

    //  a STX c d ETX f g
    // 0 1   2 3 4   5 6 7

    public void LeftArrow()
    {
        if (caretTracker == 0)
            return;

        caretTracker--;

        if (text[caretTracker] == Display.ETX)
        {
            // Move caret to start of respective operator (at STX)
            while (text[caretTracker] != Display.STX)
                caretTracker--;
        }
    }

    public void RightArrow()
    {
        if (caretTracker == text.Length)
            return;

        caretTracker++;

        if (text[caretTracker - 1] == Display.STX)
        {
            // Move caret to end of respective operator (after ETX)
            while (text[caretTracker] != Display.ETX)
                caretTracker++;

            caretTracker++;
        }
    }


    public override void OnDeselect(BaseEventData a)
    {

    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        caretPosition = 3;
    }
}

