﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class LargeOperatorNavigator : MonoBehaviour
{
    private InputManager inputManager;

    private Navigator navigator;

    public List<InputField> ArgumentInputFieldList;
    public int selectedInputField = 0;

    EventSystem system;
    public int caretPreviousPosition = -1;

    void Start()
    {
        system = EventSystem.current;
        inputManager = GameObject.Find("InputManager").GetComponent<InputManager>();
    }

    void Enter(Navigator navigator, bool startFromLast)
    {
        this.navigator = navigator;

        int inputField = (startFromLast) ? ArgumentInputFieldList.Count - 1 : 0;

        int caretPosition = (startFromLast) ? ArgumentInputFieldList[inputField].text.Length : 0;

        inputManager.SetAsSelectedInputField(ArgumentInputFieldList[inputField], caretPosition);
    }

    void Exit(bool moveOn = false)
    {
        if (navigator != null)  
            navigator.Enter(moveOn);
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.LeftArrow) && ArgumentInputFieldList[selectedInputField].isFocused == true 
            && ArgumentInputFieldList[selectedInputField].caretPosition == 0 && caretPreviousPosition == 0)
        {
            Debug.Log("LON - Left");

            // Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
            if (selectedInputField <= 0)
                return;

            InputField nextInputField = ArgumentInputFieldList[selectedInputField - 1];

            if (nextInputField != null)
            {
                selectedInputField--;
                inputManager.SetAsSelectedInputField(ArgumentInputFieldList[selectedInputField], ArgumentInputFieldList[selectedInputField].text.Length);

                return;
            }
        }

        if (UnityEngine.Input.GetKeyDown(KeyCode.RightArrow) && ArgumentInputFieldList[selectedInputField].isFocused == true 
            && ArgumentInputFieldList[selectedInputField].caretPosition == ArgumentInputFieldList[selectedInputField].text.Length 
            && caretPreviousPosition == ArgumentInputFieldList[selectedInputField].text.Length)
        {
            // Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
            Debug.Log("LON - Right");
            if (selectedInputField >= ArgumentInputFieldList.Count - 1)
                return;

            InputField nextInputField = ArgumentInputFieldList[selectedInputField + 1];

            if (nextInputField != null)
            {
                selectedInputField++;
                inputManager.SetAsSelectedInputField(ArgumentInputFieldList[selectedInputField]);
                return;
            }
        }

        caretPreviousPosition = ArgumentInputFieldList[selectedInputField].caretPosition;
    }
}