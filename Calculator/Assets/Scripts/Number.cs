﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Number : Item
{
    public double Value { get; protected set; } 

    public Number(double d)
    {
        Value = d;
    }

    public override object Clone()
    {
        return new Number(Value);
    }

    public override string Display()
    {
        return Value.ToString();
    }
}

public class E : Number
{
    public E() : base(Math.E)
    {

    }

    public override object Clone()
    {
        return new E();
    }
}

public class Pi : Number
{
    public Pi() : base(Math.PI)
    {

    }

    public override object Clone()
    {
        return new Pi();
    }
}

public class X : Number
{
    public double X_Value { get; set; }

    public X() : base(7)  // Base will be ignored when graph is called
    {

    }

    public override object Clone()
    {
        return new X();
    }
}