﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GlobalSpace;

public class ParenOpen : Item
{
    public override string Display()
    {
        return "(";
    }

    public override object Clone()
    {
        return new ParenOpen();
    }
}

public class ParenClose : Item
{
    public override string Display()
    {
        return ")";
    }

    public override object Clone()
    {
        return new ParenClose();
    }
}
